/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.util.ArrayList;
/**
 *
 * @author hp
 */
public class MySym extends java_cup.runtime.DefaultSymbolFactory{
    
    
    public static final String TYPE_CHAR   = "CHAR";
    public static final String TYPE_FLOAT  = "FLOAT";
    public static final String TYPE_VOID   = "VOID";
    public static final String TYPE_STRING = "STRING";
    public static final String TYPE_BOOLEAN= "BOOLEAN";
    public static final String TYPE_INT    = "INT";
    
    public static final String ROL_CLASS      ="CLASE";
    public static final String ROL_ATRIBUTO   ="ATRIBUTO";
    public static final String ROL_VAR        ="VARIABLE";
    public static final String ROL_LOCAL_VAR  ="VARIABLE LOCAL";
    public static final String ROL_METHOD     ="METHOD";
    public static final String ROL_FUNCTION     ="FUNCTION";
    public static final String ROL_PARAMETRO  ="PARAMETRO";
    public static final String ROL_CONSTRUCTOR ="CONSTRUCTOR";
    public static final String ROL_RETURN      ="RETURN";
    public static final String ROL_MAIN      ="MAIN";

    public static final String VISIBILITY_PUBLIC    ="PUBLIC";
    public static final String VISIBILITY_PRIVATE   ="PRIVATE";
    public static final String VISIBILITY_PROTECTED ="PROTECTED";
    
    public static final String VAR_BY_REFERENCE ="REFERENCIA";
    public static final String VAR_BY_VALUE     ="VALOR";
    
    
    /**
     * Devuelve el nombre de las propiedades de la clase MySym
     * @return 
     */
    public static String[] getProperties() {
        return new String[]{"nombre","ambito","visiblidad","tipo","rol","Tamaño","Posición"};
    }

   
    
    String  nombre      =null;
    String  ambito      =null;
    String  visibilidad =null;
    String  tipo        =null;
    String  rol         =null;
    Integer size        =null;
    Integer posicion    =null;
    
    //clases
    String class_padre  =null;
    ArrayList<MySym> attr_lista=null;
    ArrayList<MySym> method_lista=null;
    
    //metodos
    /**
     * parametro por valor o referencia
     */
    String by =null;
    ArrayList<MySym> param_lista=null;
    ArrayList<MySym> var_lista=null;
    String tres_nombre=null;
    
    //arreglos
    ArrayList<Integer[]> dimension=null;
    boolean arreglo_de_arreglo=false;
    //objetos
    String def_tres="";
    
    
    
    
    public MySym(String nombre,String ambito,String visibilidad,String tipo,String rol,Integer size,Integer puntero) {
        this.nombre = nombre;
        this.ambito = ambito;
        this.visibilidad = visibilidad;
        this.tipo = tipo;
        this.rol = rol;
        this.size=size;
        this.posicion = puntero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public Integer getSize() {
        return size;
    }

    public ArrayList<MySym> getParam_lista() {
        return param_lista;
    }

    public void setParam_lista(ArrayList<MySym> param_lista) {
        this.param_lista = param_lista;
    }
    public int getParam_size(){
        if(param_lista!=null){
            return param_lista.size();
        }
        else
            return 0;
    }

    public String getTres_nombre() {
        return tres_nombre;
    }

    public void setTres_nombre(String tres_nombre) {
        this.tres_nombre = tres_nombre;
    }

    public String getDef_tres() {
        return def_tres;
    }

    public void setDef_tres(String def_tres) {
        this.def_tres = def_tres;
    }

  

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getVisibilidad() {
        return visibilidad;
    }

    public void setVisibilidad(String visibilidad) {
        this.visibilidad = visibilidad;
    }

   
    public String getTipos(){
        
        ArrayList<String> tipos=new ArrayList<>();
        if(rol.equals(MySym.ROL_CLASS)||rol.equals(MySym.ROL_ATRIBUTO))
                return "";
        
        //todos lo methods utilizan una lista para los tipos
        if(param_lista!=null)
            for (MySym sym : param_lista) {
                tipos.add(sym.tipo);
            }
        return tipos.toString();
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public boolean isArreglo_de_arreglo() {
        return arreglo_de_arreglo;
    }

    public void setArreglo_de_arreglo(boolean arreglo_de_arreglo) {
        this.arreglo_de_arreglo = arreglo_de_arreglo;
    }

    

    /**
     * Devuelve un arreglo con los valores, el orden es segun getProperties()
     * @return 
     */
    public String[] getValues() {
        
        
//        String v=MySym.getVisibilityName(this.visibilidad);
//        String t =MySym.getTipoName(this.tipo);
//        String r=MySym.getRolName(this.rol);
        String v=this.visibilidad;
        String t=this.tipo.toString();
        String r=this.rol;
        String s=this.size.toString();
        String p=this.posicion.toString();
                
        if(this.size==-1)
            s="";
        if(this.posicion==-1)
            p="";
                
        String[] val= new String[]{this.nombre,this.ambito,v,t,r,s,p};
        
        
        return val;
    }
    
    
    //10/05/2012

    public String getClass_padre() {
        return class_padre;
    }

    public void setClass_padre(String class_padre) {
        this.class_padre = class_padre;
    }

    public ArrayList<Integer[]> getDimension() {
        return dimension;
    }

    public void setDimension(ArrayList<Integer[]> dimension) {
        this.dimension = dimension;
    }

   

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }
    
    
    public static boolean isVarType(String tipo){
        boolean r=true;
        switch (tipo) {
            case MySym.TYPE_BOOLEAN:
                break;
            case MySym.TYPE_CHAR:
                break;
            case MySym.TYPE_FLOAT:
            break;
            case MySym.TYPE_INT:
                break;
            case MySym.TYPE_STRING:
                break;
            default:
                r=false;
        }
       return r;
    }

    public ArrayList<MySym> getAttr_lista() {
        return attr_lista;
    }

    public void setAttr_lista(ArrayList<MySym> attr_lista) {
        this.attr_lista = attr_lista;
    }

    public ArrayList<MySym> getVar_lista() {
        return var_lista;
    }

    public void setVar_lista(ArrayList<MySym> local_var_lista) {
        this.var_lista = local_var_lista;
    }

    public ArrayList<MySym> getMethod_lista() {
        return method_lista;
    }

    public void setMethod_lista(ArrayList<MySym> method_lista) {
        this.method_lista = method_lista;
    }
    public MySym getMethodVarSym(String nombre){
                
        if (this.var_lista!=null&&!this.var_lista.isEmpty())
            for(MySym sym: this.var_lista ){
                if(sym.getNombre().equals(nombre))
                    return sym;
            }
               
        return null;
    }
    //clases
    
    public MySym getAttrSym(String nombre){
                
        if (this.attr_lista!=null&&!this.attr_lista.isEmpty())
            for(MySym sym: this.attr_lista ){
                if(sym.getNombre().equals(nombre)&&sym.getRol().equals(MySym.ROL_ATRIBUTO))
                    return sym;
            }
               
        return null;
    }
   
    public MySym getMethodSym(String nombre,String tipos){
                
        if (this.method_lista!=null&&!this.method_lista.isEmpty())
            for(MySym sym: this.attr_lista ){
                final String rol1 = sym.getRol();
                if  (
                        sym.getNombre().equals(nombre)&&
                        (rol1.equals(MySym.ROL_METHOD)||rol1.equals(MySym.ROL_FUNCTION)||rol1.equals(MySym.ROL_CONSTRUCTOR))&&
                        tipos.equals(sym.getTipos())
                    )
                    return sym;
            }
               
        return null;
    }
    
}
