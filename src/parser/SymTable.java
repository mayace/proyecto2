/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 *
 * @author hp
 */
public class SymTable {
    /**
     * Contiene símbolos realmente almacenados
     */
    HashMap<String, MySym> tabla=new HashMap<>();
    /**
     * Contiene símbolos almacenados temporalmente
     * Estos se guardan realmente con un llamado al commit()
     */
    HashMap<String, MySym> cache=new HashMap<>();
    
    /**
     * Contienen el apuntador a la tabla en uso, cache o tabla
     * Por defecto es en tabla
     */
    HashMap<String, MySym> principal=tabla;
    
    /**
     * Agrega un nuevo simbolo a la tabla de simbolos
     * @param nombre
     * @param ambito
     * @param visibilidad
     * @param tipo
     * @param rol
     * @param size
     * @param posicion 
     * @param tipos Representa los tipos de sus parametros si es méotodo...
     * @return Retorna el Símbolo agregado...
     */
    public MySym addSymbol(String nombre, String ambito, String visibilidad, String tipo, String rol,Integer size, Integer posicion,String tipos){
        
        return addSymbolCore(this.principal, nombre, ambito, visibilidad, tipo, rol, size, posicion, tipos);
        
    }
    
    
     public MySym addSymbolCore(HashMap<String,MySym> tabla,String nombre, String ambito, String visibilidad, String tipo, String rol,Integer size, Integer posicion,String tipos){
        

        MySym s=new MySym(nombre, ambito,visibilidad, tipo, rol,size, posicion);
        String key=getKey(nombre, ambito,tipos);
        MySym sym_anterior = tabla.put(key, s);
        MySym sym_inserted = tabla.get(key);
        
        return sym_inserted;
        
    }
    
    /**
     * Genera un llave para obtener el un simbolo de la tabla
     * @param nombre
     * @param ambito
     * @return key is : nombre+"/"+ambito+"/"+tipo 
     */
    String getKey(String nombre, String ambito,String tipos){
        return ambito+"/"+nombre+"/"+tipos;
    }

  
    
    public MySym addMethod(String nombre,String ambito,String visibility,String tipo,Integer size,String tipos){
        MySym sym = this.addSymbol(nombre, ambito, visibility, tipo, MySym.ROL_METHOD, size, -1,tipos);
        return sym;
    }
    
  
    public MySym addVar (
                        String nombre,
                        String ambito,
                        String visibilidad,
                        String tipo,
                        String rol,
                        int posicion,
                        Integer size,
                        String tipos
                        ){
        
        MySym sym = this.addSymbol(nombre, ambito, visibilidad, tipo,rol, size, posicion,tipos);
    
        return sym;   
    }
    
    
    public MySym addArreglo (
                        String nombre,
                        String ambito,
                        String visibilidad,
                        String tipo,
                        String rol,
                        int posicion,
                        int size,
                        ArrayList<Integer[]> dimension,
                        String tipos
                        ){
        
        
        MySym sym = this.addSymbol(nombre, ambito, visibilidad, tipo,rol, size, posicion,tipos);
        sym.setDimension(dimension);
        return sym;    
            
    }
    
    
    /**
     * Agrega un símbolo con rol clase a la taba de símbolos
     * @param nombre
     * @param padre
     * @param size 
     */
    public MySym addClass(String nombre,String padre,int size){
        /**
         * Información básica
         */
        MySym sym = this.addSymbol(nombre, "", "", "", MySym.ROL_CLASS, size, -1,"");
        /**
         * Informació exclusiva para clases
         */
        sym.setClass_padre(padre);
        return sym;
    }
    
    public MySym getSymBoth(String nombre,String ambito,String tipos){
        MySym sym = getSymTabla(nombre, ambito, tipos);
        MySym symCache = getSymCache(nombre, ambito, tipos);
        
        if(sym!=null)
            return sym;
        else if(symCache!=null)
            return symCache;
        else
            return null;
    }
    
    /**
     * Retorna el símbolo en un ambito especifico...
     * @param nombre
     * @param ambito
     * @return null encaso de que no se encuentre...
     */
    public MySym getSym(String nombre,String ambito,String tipos){
        return getSymCore(this.principal, nombre, ambito, tipos);
    }
    
    public MySym getSymCache(String nombre,String ambito,String tipos){
        return getSymCore(this.cache, nombre, ambito, tipos);
    }
    public MySym getSymTabla(String nombre,String ambito,String tipos){
        return getSymCore(this.tabla, nombre, ambito, tipos);
    }
    
    public MySym getSymCore(HashMap<String,MySym> tabla,String nombre,String ambito,String tipos){
        MySym sym=null;
        String key=this.getKey(nombre, ambito,tipos);
        if(tabla.containsKey(key)){
            sym=tabla.get(key);
        }
        
        return sym;
    }
    
    /**
     * Retorna el tipo de un símbolo
     * @param nombre
     * @param ambito
     * @return null en caso de que no exista o no es una variable
     */
    public String getSymType(String nombre,String ambito,String tipos){
        String tipo=null;
        MySym sym = getSym(nombre, ambito,tipos);
        
        if(sym!=null){
            tipo=sym.tipo;
        }
        
        return tipo;
    }
    
    public boolean exists_symBoth(String nombre, String ambito,String tipos) {
        return exists_symCore(this.tabla,nombre, ambito, tipos)|| exists_symCache(nombre, ambito, tipos);
    }
    /**
     * Verifica si existe un simbolo
     * @param nombre
     * @param ambito
     * @return 
     */
    public boolean exists_sym(String nombre, String ambito,String tipos) {
        return exists_symCore(this.principal, nombre, ambito, tipos);
    }
    
    public boolean exists_symCache(String nombre, String ambito,String tipos) {
        return exists_symCore(this.cache, nombre, ambito, tipos);
    }
    
    
    public boolean exists_symCore(HashMap<String,MySym> tabla,String nombre, String ambito,String tipos) {
        MySym sym = getSymCore(tabla,nombre, ambito,tipos);
        if(sym==null)
            return false;
        else
            return true;
    }
    
    public MySym addParametro   (
                                    String nombre,
                                    String ambito,
                                    String tipo,
                                    int posicion,
                                    String by,
                                    String tipos
                                ){
        MySym sym = addVar(nombre, ambito, "", tipo, MySym.ROL_PARAMETRO, posicion, 1,tipos);
        
        sym.setBy(by);
        
        return sym;
    }
    
    public boolean commit(){
        boolean r=true;
        if(!cache.isEmpty()){
            try {
                tabla.putAll(cache);
                cache.clear();
            } catch (Exception exc) {
                return false;
            }   
        }
        return r;
    }
    
    public void clearCache(){
        this.cache.clear();
    }
    
    public void setPrincipal_cache(){
        this.principal=this.cache;
    }
    public void setPrincipal_tabla(){
        this.principal=this.tabla;
    }

    /**
     * Retorna la tabla real
     * @return tabla
     */
    public HashMap<String, MySym> getTabla() {
        return tabla;
    }
    
    /**
     * Retorna el primer método que concida
     * No toma en cuenta 'tipos', solo nombre y ambito
     * @param nombre
     * @param clase
     * @return 
     */
    public boolean exists_method(String nombre,String clase){
        boolean r=false;
        Collection<MySym> values = this.principal.values();
        for (MySym sym : values) {
            if(sym.getNombre().equals(nombre)&&sym.getAmbito().equals(clase))
                return true;
        }
        
        return r;
    }
    
   public boolean exists_constructor(String clase){
       return exists_method(clase, clase);
   }
   
   /**
    * Returna una lista de atributos de una clase, busca en las dos map 
    * @param clase
    * @return 
    */
   public ArrayList<MySym> getAttributes(String clase){
       ArrayList<MySym> r=new ArrayList<>();
       
       
       if(tabla.containsKey(getKey(clase, "", "")))
           for (MySym sym : tabla.values()) {
               if(sym.getAmbito().equals(clase)&&sym.getRol().equals(MySym.ROL_ATRIBUTO))
                    r.add(sym);
           }
       else if(cache.containsKey(getKey(clase, "", "")))
           for (MySym sym : cache.values()) {
               if(sym.getAmbito().equals(clase)&&sym.getRol().equals(MySym.ROL_ATRIBUTO))
                    r.add(sym);
           }
       
       return r;
   }
   
    
}
