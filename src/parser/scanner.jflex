package parser;

import java_cup.runtime.Symbol;
import javax.swing.JTextArea;

%%

%class Scanner
%public
%line
%column
%cup


%{
	
	
	JTextArea salida=new JTextArea();
	public void setSalida(JTextArea salida){
		this.salida=salida;
	}
	
	public JTextArea getSalida(){
		return this.salida;
	}
	
	public void out(String text){
		this.salida.append(text+"\n");
	}
	
    /* To create a new java_cup.runtime.Symbol with information about
       the current token, the token will have no value in this
       case. */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}
NEWLINE		=\n|\r|\r\n
ESPACIO     =[ \t]|{NEWLINE}
LETRA		=[a-z�A-Z�]
DIGITO		=[0-9]
ID			={LETRA}({LETRA}|{DIGITO}|"_")*
NUMBER		={DIGITO}+("."{DIGITO}+)?
SIMPLE_C	="!"[^\n\r(\r\n)]*{NEWLINE}
MULTI_C		="!!!"[^]*"!!!"
CHAR		="'"({LETRA}|{DIGITO})"'"
BOOLEAN		="true"|"false"
STRING		=\"[^\"\n\r]*\"

%%

<YYINITIAL>{
	{ESPACIO}		{/*nada*/}
	{SIMPLE_C}		{/*COMENTARIO*/}
	{MULTI_C}		{/*COMENTARIO*/}
	"class"			{return symbol(Sym.CLASS,yytext());}
	"inherits"		{return symbol(Sym.INHERITS,yytext());}
	"@Override"		{return symbol(Sym.OVERRIDE,yytext());}
	"end"			{return symbol(Sym.END,yytext());}
	"new"			{return symbol(Sym.NEW,yytext());}
	
	"#include"		{return symbol(Sym.INCLUDE,yytext());}
	"<"				{return symbol(Sym.MENORQ,yytext());}
	">"				{return symbol(Sym.MAYORQ,yytext());}
	"."				{return symbol(Sym.DOT,yytext());}
	":"				{return symbol(Sym.DPUNTOS,yytext());}
	","				{return symbol(Sym.COMA,yytext());}
	"*"				{return symbol(Sym.ASTERISCO,yytext());}
	"("				{return symbol(Sym.PAR1,yytext());}
	")"				{return symbol(Sym.PAR2,yytext());}
	"["				{return symbol(Sym.CORCHETE1,yytext());}
	"]"				{return symbol(Sym.CORCHETE2,yytext());}
	
	"+"				{return symbol(Sym.SUMA,yytext());}
	"-"				{return symbol(Sym.RESTA,yytext());}
	"/"				{return symbol(Sym.DIVISION,yytext());}	
	"%"				{return symbol(Sym.MOD,yytext());}	
	
	"defvar"		{return symbol(Sym.DEFVAR,yytext());}
	"int"			{return symbol(Sym.INT_TYPE,yytext());}
	"float"			{return symbol(Sym.FLOAT_TYPE,yytext());}
	"string"		{return symbol(Sym.STRING_TYPE,yytext());}
	"char"			{return symbol(Sym.CHAR_TYPE,yytext());}
	"boolean"		{return symbol(Sym.BOOLEAN_TYPE,yytext());}
	"void"			{return symbol(Sym.VOID_TYPE,yytext());}
	"return"		{return symbol(Sym.RETURN,yytext());}
	
	"public"		{return symbol(Sym.PUBLIC,yytext());}
	"protected"		{return symbol(Sym.PROTECT,yytext());}
	"private"		{return symbol(Sym.PRIVATE,yytext());}
	
	"def"			{return symbol(Sym.DEF,yytext());}
	"setf"			{return symbol(Sym.SETF,yytext());}
	"reference"		{return symbol(Sym.REFERENCE,yytext());}
	
	{BOOLEAN}		{return symbol(Sym.BOOLEAN,yytext());}
	{ID}			{return symbol(Sym.ID,yytext());}
	{NUMBER}		{return symbol(Sym.NUM,yytext());}
	{CHAR}			{return symbol(Sym.CHAR,yytext());}
	{STRING}		{return symbol(Sym.STRING,yytext());}
	
}

<<EOF>>				{return symbol(Sym.EOF,yytext());}

[^]		{salida.append("[Error] Illegal character <"+yytext()+"> at line "+(yyline+1)+" column " +(yycolumn+1)+"\n");}