package parser.tres;

import java_cup.runtime.Symbol;
import javax.swing.JTextArea;

%%

%class TresScanner2
%public
%line
%column
%cup


%{
	
	
	JTextArea salida=new JTextArea();
	public void setSalida(JTextArea salida){
		this.salida=salida;
	}
	
	public JTextArea getSalida(){
		return this.salida;
	}
	
	public void out(String text){
		this.salida.append(text+"\n");
	}
	
    /* To create a new java_cup.runtime.Symbol with information about
       the current token, the token will have no value in this
       case. */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}

NEWLINE		=\n|\r|\r\n
ESPACIO     =[ \t]|{NEWLINE}
LETRA		=[a-z�A-Z�]
DIGITO		=[0-9]

TAG			=("l"|"L"){INT}
GOTO		=("GOTO"|"goto"|"Goto")
TEMPORAL	=("t"|"T"){INT}
STACK		="STACK"|"stack"|"Stack"
HEAP		="HEAP"|"heap"|"Heap"
P			="P"|"p"
H			="H"|"h"

ID			={LETRA}({LETRA}|{DIGITO}|"_")*
INT			=("-")?{DIGITO}+
FLOAT		={INT}"."{INT}
CHAR		="'"({LETRA}|{DIGITO})"'"
BOOLEAN		="true"|"false"
STRING		=\"[^\"\n\r]*\"

SIMPLE_C	="//"[^\r\n]*{NEWLINE}
MULTI_C		="/*"[^\*\/]*"*/" 

%%

<YYINITIAL>{
	
	{ESPACIO}		{/*nada*/}
	{SIMPLE_C}		{/*COMENTARIO*/}
	{MULTI_C}		{/*COMENTARIO*/}
	
	
	
	"<"				{return symbol(TresSym2.MENOR,yytext());}
	">"				{return symbol(TresSym2.MAYOR,yytext());}
	">="			{return symbol(TresSym2.MAYOR_IGUAL,yytext());}
	"<="			{return symbol(TresSym2.MENOR_IGUAL,yytext());}
	"!="			{return symbol(TresSym2.NOIGUAL,yytext());}
	"=="			{return symbol(TresSym2.IGUAL2,yytext());}
	
	";"				{return symbol(TresSym2.PCOMA,yytext());}
	","				{return symbol(TresSym2.COMA,yytext());}
	"="				{return symbol(TresSym2.IGUAL,yytext());}
	
	"("				{return symbol(TresSym2.PAR1,yytext());}
	")"				{return symbol(TresSym2.PAR2,yytext());}
	"["				{return symbol(TresSym2.CORCHETE1,yytext());}
	"]"				{return symbol(TresSym2.CORCHETE2,yytext());}
	
	"*"				{return symbol(TresSym2.MULTI,yytext());}
	"+"				{return symbol(TresSym2.SUMA,yytext());}
	"-"				{return symbol(TresSym2.RESTA,yytext());}
	"/"				{return symbol(TresSym2.DIVISION,yytext());}	
	"%"				{return symbol(TresSym2.MOD,yytext());}	
	
	"if"			{return symbol(TresSym2.IF,yytext());}
	
	{TAG}			{return symbol(TresSym2.TAG,yytext());}
	{GOTO}			{return symbol(TresSym2.GOTO,yytext());}
	{TEMPORAL}		{return symbol(TresSym2.TEMP,yytext());}
	{P}				{return symbol(TresSym2.P,yytext());}
	{H}				{return symbol(TresSym2.H,yytext());}
	{STACK}			{return symbol(TresSym2.STACK,yytext());}
	{HEAP}			{return symbol(TresSym2.HEAP,yytext());}
	
	
	{BOOLEAN}		{return symbol(TresSym2.BOOLEAN,yytext());}
	"null"			{return symbol(TresSym2.NULL,yytext());}
	{ID}			{return symbol(TresSym2.ID,yytext());}
	{INT}			{return symbol(TresSym2.INT,yytext());}
	{FLOAT}			{return symbol(TresSym2.FLOAT,yytext());}
	{CHAR}			{return symbol(TresSym2.CHAR,yytext());}
	{STRING}		{return symbol(TresSym2.STRING,yytext());}
	
	

}

<<EOF>>				{return symbol(TresSym2.EOF,yytext());}

[^]		{salida.append("[Error] Illegal character <"+yytext()+"> at line "+(yyline+1)+" column " +(yycolumn+1)+"\n");}