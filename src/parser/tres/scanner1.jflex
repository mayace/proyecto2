package parser.tres;

import java_cup.runtime.Symbol;
import javax.swing.JTextArea;

%%

%class TresScanner1
%public
%line
%column
%cup
%state S1

%{
	
	
	JTextArea salida=new JTextArea();
	public void setSalida(JTextArea salida){
		this.salida=salida;
	}
	
	public JTextArea getSalida(){
		return this.salida;
	}
	
	public void out(String text){
		this.salida.append(text+"\n");
	}
	
	StringBuffer body = new StringBuffer();
	String tag=null;
	boolean ok=false;

	
    /* To create a new java_cup.runtime.Symbol with information about
       the current token, the token will have no value in this
       case. */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}

NEWLINE		=\n|\r|\r\n
ESPACIO     =[ \t]|{NEWLINE}
LETRA		=[a-z�A-Z�]
DIGITO		=[0-9]



INT			={DIGITO}+
TAG			=("L"|"l"){INT}":"
BODY		={LETRA}+[^\:]+";"

SIMPLE_C	="//"[^\r\n]*{NEWLINE}
MULTI_C		="/*"[^\*\/]*"*/" 


%%

<YYINITIAL>{
	
	{ESPACIO}		{}
	{SIMPLE_C}		{/*COMENTARIO*/}
	{MULTI_C}		{/*COMENTARIO*/}
	

	{TAG}			{
						// salida.append("tag  "+yytext()+"\n"); 
						return symbol(TresSym1.TAG,yytext());
							
					}
	{BODY}			{
						
						// salida.append("body  "+yytext()+"\n"); 
						return symbol(TresSym1.BODY,yytext());
					}
	
}

<<EOF>>				{return symbol(TresSym1.EOF,yytext());}

[^]		{salida.append("[Error] Illegal character <"+yytext()+"> at line "+(yyline+1)+" column " +(yycolumn+1)+"\n");}