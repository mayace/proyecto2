
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Sat May 26 13:12:55 CST 2012
//----------------------------------------------------

package parser.tres;

/** CUP generated class containing symbol constants. */
public class TresSym {
  /* terminals */
  public static final int ID = 2;
  public static final int error = 1;
  public static final int BODY = 3;
  public static final int PAR2 = 6;
  public static final int VOID = 4;
  public static final int EOF = 0;
  public static final int PAR1 = 5;
}

