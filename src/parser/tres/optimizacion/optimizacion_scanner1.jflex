package parser.tres.optimizacion;

import java_cup.runtime.Symbol;
import javax.swing.JTextArea;

%%

%class OptimizacionScanner1
%public
%line
%column
%cup
%state STRING

%{
	
	StringBuffer string = new StringBuffer();
	JTextArea salida=new JTextArea();
	public void setSalida(JTextArea salida){
		this.salida=salida;
	}
	
	public JTextArea getSalida(){
		return this.salida;
	}
	
	public void out(String text){
		this.salida.append(text+"\n");
	}
	
	StringBuffer body = new StringBuffer();
	String tag=null;
	boolean ok=false;

	
    /* To create a new java_cup.runtime.Symbol with information about
       the current token, the token will have no value in this
       case. */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}

NEWLINE		=\n|\r|\r\n
ESPACIO     =[ \t]|{NEWLINE}
LETRA		=[a-z�A-Z�]
DIGITO		=[0-9]




BODY		="/*{*/"[^\*\/]*"/*}*/" 

SIMPLE_C	="//"[^\r\n]*{NEWLINE}
MULTI_C		="/*"[^\*\/]*"*/" 


%%

<YYINITIAL>{
	
	{ESPACIO}		{}

	"/*{*/"			{ string.setLength(0); yybegin(STRING);}
	
}
<STRING> {
  "/*}*/"       { 
					yybegin(YYINITIAL);
					return symbol(OptimizacionSym.BODY, string.toString()); 
				}
  [^\}]*        { string.append( yytext() ); }
  
}

<<EOF>>				{return symbol(OptimizacionSym1.EOF,yytext());}

[^]		{salida.append("[Error] Illegal character <"+yytext()+"> at line "+(yyline+1)+" column " +(yycolumn+1)+"\n");}