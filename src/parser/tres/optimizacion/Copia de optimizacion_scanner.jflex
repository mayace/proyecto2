package parser.tres.optimizacion;

import java_cup.runtime.Symbol;
import javax.swing.JTextArea;

%%

%class OptimizacionScanner
%public
%line
%column
%cup


%{
	
	
	JTextArea salida=new JTextArea();
	public void setSalida(JTextArea salida){
		this.salida=salida;
	}
	
	public JTextArea getSalida(){
		return this.salida;
	}
	
	public void out(String text){
		this.salida.append(text+"\n");
	}
	
    /* To create a new java_cup.runtime.Symbol with information about
       the current token, the token will have no value in this
       case. */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}

NEWLINE		=\n|\r|\r\n
ESPACIO     =[ \t]|{NEWLINE}
LETRA		=[a-z�A-Z�]
DIGITO		=[0-9]

TAG			=("l"|"L"){INT}
GOTO		=("GOTO"|"goto"|"Goto")
TEMPORAL	=("t"|"T"){INT}
STACK		="STACK"|"stack"|"Stack"
HEAP		="HEAP"|"heap"|"Heap"
P			="P"|"p"
H			="H"|"h"

ID			={LETRA}({LETRA}|{DIGITO}|"_")*
INT			=("-")?{DIGITO}+
FLOAT		={INT}"."{INT}
CHAR		="'"({LETRA}|{DIGITO})"'"
BOOLEAN		="true"|"false"
STRING		=\"[^\"\n\r]*\"

SIMPLE_C	="//"[^\r\n]*{NEWLINE}
MULTI_C		="/*"[^\*\/]*"*/" 

%%

<YYINITIAL>{
	
	{ESPACIO}		{/*nada*/}
	{SIMPLE_C}		{/*COMENTARIO*/}
	{MULTI_C}		{/*COMENTARIO*/}
	
	
	
	"<"				{return symbol(OptimizacionSym.MENOR,yytext());}
	">"				{return symbol(OptimizacionSym.MAYOR,yytext());}
	">="			{return symbol(OptimizacionSym.MAYOR_IGUAL,yytext());}
	"<="			{return symbol(OptimizacionSym.MENOR_IGUAL,yytext());}
	"!="			{return symbol(OptimizacionSym.NOIGUAL,yytext());}
	"=="			{return symbol(OptimizacionSym.IGUAL2,yytext());}
	
	";"				{return symbol(OptimizacionSym.PCOMA,yytext());}
	","				{return symbol(OptimizacionSym.COMA,yytext());}
	"="				{return symbol(OptimizacionSym.IGUAL,yytext());}
	
	"{"				{return symbol(OptimizacionSym.LLAVE1,yytext());}
	"}"				{return symbol(OptimizacionSym.LLAVE2,yytext());}
	"("				{return symbol(OptimizacionSym.PAR1,yytext());}
	")"				{return symbol(OptimizacionSym.PAR2,yytext());}
	"["				{return symbol(OptimizacionSym.CORCHETE1,yytext());}
	"]"				{return symbol(OptimizacionSym.CORCHETE2,yytext());}
	
	"*"				{return symbol(OptimizacionSym.MULTI,yytext());}
	"+"				{return symbol(OptimizacionSym.SUMA,yytext());}
	"-"				{return symbol(OptimizacionSym.RESTA,yytext());}
	"/"				{return symbol(OptimizacionSym.DIVISION,yytext());}	
	"%"				{return symbol(OptimizacionSym.MOD,yytext());}	
	
	"if"			{return symbol(OptimizacionSym.IF,yytext());}
	
	{TAG}			{return symbol(OptimizacionSym.TAG,yytext());}
	{GOTO}			{return symbol(OptimizacionSym.GOTO,yytext());}
	{TEMPORAL}		{return symbol(OptimizacionSym.TEMP,yytext());}
	{P}				{return symbol(OptimizacionSym.P,yytext());}
	{H}				{return symbol(OptimizacionSym.H,yytext());}
	{STACK}			{return symbol(OptimizacionSym.STACK,yytext());}
	{HEAP}			{return symbol(OptimizacionSym.HEAP,yytext());}
	
	
	
	"void"			{return symbol(OptimizacionSym.VOID,yytext());}
	{BOOLEAN}		{return symbol(OptimizacionSym.BOOLEAN,yytext());}
	{ID}			{return symbol(OptimizacionSym.ID,yytext());}
	{INT}			{return symbol(OptimizacionSym.INT,yytext());}
	{FLOAT}			{return symbol(OptimizacionSym.FLOAT,yytext());}
	{CHAR}			{return symbol(OptimizacionSym.CHAR,yytext());}
	{STRING}		{return symbol(OptimizacionSym.STRING,yytext());}
	
	

}

<<EOF>>				{return symbol(OptimizacionSym.EOF,yytext());}

[^]		{salida.append("[Error] Illegal character <"+yytext()+"> at line "+(yyline+1)+" column " +(yycolumn+1)+"\n");}