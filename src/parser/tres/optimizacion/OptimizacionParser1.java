
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Mon May 28 01:19:06 CST 2012
//----------------------------------------------------

package parser.tres.optimizacion;

import java_cup.runtime.Symbol;
import javax.swing.JTextArea;
import java.util.HashMap;
import parser.MyClass;

/** CUP v0.11a beta 20060608 generated parser.
  * @version Mon May 28 01:19:06 CST 2012
  */
public class OptimizacionParser1 extends java_cup.runtime.lr_parser {

  /** Default constructor. */
  public OptimizacionParser1() {super();}

  /** Constructor which sets the default scanner. */
  public OptimizacionParser1(java_cup.runtime.Scanner s) {super(s);}

  /** Constructor which sets the default scanner. */
  public OptimizacionParser1(java_cup.runtime.Scanner s, java_cup.runtime.SymbolFactory sf) {super(s,sf);}

  /** Production table. */
  protected static final short _production_table[][] = 
    unpackFromStrings(new String[] {
    "\000\007\000\002\006\002\000\002\002\004\000\002\002" +
    "\004\000\002\003\003\000\002\004\004\000\002\004\003" +
    "\000\002\005\003" });

  /** Access to production table. */
  public short[][] production_table() {return _production_table;}

  /** Parse-action table. */
  protected static final short[][] _action_table = 
    unpackFromStrings(new String[] {
    "\000\011\000\004\004\001\001\002\000\004\004\011\001" +
    "\002\000\004\002\006\001\002\000\004\002\uffff\001\002" +
    "\000\006\002\ufffc\004\ufffc\001\002\000\006\002\ufffe\004" +
    "\011\001\002\000\006\002\ufffb\004\ufffb\001\002\000\004" +
    "\002\000\001\002\000\006\002\ufffd\004\ufffd\001\002" });

  /** Access to parse-action table. */
  public short[][] action_table() {return _action_table;}

  /** <code>reduce_goto</code> table. */
  protected static final short[][] _reduce_table = 
    unpackFromStrings(new String[] {
    "\000\011\000\006\002\004\006\003\001\001\000\010\003" +
    "\011\004\007\005\006\001\001\000\002\001\001\000\002" +
    "\001\001\000\002\001\001\000\004\005\012\001\001\000" +
    "\002\001\001\000\002\001\001\000\002\001\001" });

  /** Access to <code>reduce_goto</code> table. */
  public short[][] reduce_table() {return _reduce_table;}

  /** Instance of action encapsulation class. */
  protected CUP$OptimizacionParser1$actions action_obj;

  /** Action encapsulation object initializer. */
  protected void init_actions()
    {
      action_obj = new CUP$OptimizacionParser1$actions(this);
    }

  /** Invoke a user supplied parse action. */
  public java_cup.runtime.Symbol do_action(
    int                        act_num,
    java_cup.runtime.lr_parser parser,
    java.util.Stack            stack,
    int                        top)
    throws java.lang.Exception
  {
    /* call code in generated class */
    return action_obj.CUP$OptimizacionParser1$do_action(act_num, parser, stack, top);
  }

  /** Indicates start state. */
  public int start_state() {return 0;}
  /** Indicates start production. */
  public int start_production() {return 2;}

  /** <code>EOF</code> Symbol index. */
  public int EOF_sym() {return 0;}

  /** <code>error</code> Symbol index. */
  public int error_sym() {return 1;}



	//#####################################################################
	//
	
	HashMap<Integer,String> bloques=new HashMap<>();
	
	public HashMap<Integer,String> getBloques(){
		return this.bloques;
	}
	
	//#####################################################################
	//SALIDA...
	JTextArea salida=new JTextArea();
	public void setSalida(JTextArea salida){
		this.salida=salida;
	}
	public JTextArea getSalida(){
		return this.salida;
	}
	public void out(String text){
		this.salida.append(text+"\n");
	}
	//#####################################################################
	//ERRORES...
    public void report_error(String message, Object info) {
   
        /* Create a StringBuffer called 'm' with the string 'Error' in it. */
        StringBuffer m = new StringBuffer("[Error]");
   
        /* Check if the information passed to the method is the same
           type as the type java_cup.runtime.Symbol. */
        if (info instanceof java_cup.runtime.Symbol) {
            /* Declare a java_cup.runtime.Symbol object 's' with the
               information in the object info that is being typecasted
               as a java_cup.runtime.Symbol object. */
            java_cup.runtime.Symbol s = ((java_cup.runtime.Symbol) info);
   
            /* Check if the line number in the input is greater or
               equal to zero. */
            if (s.left >= 0) {                
                /* Add to the end of the StringBuffer error message
                   the line number of the error in the input. */
                m.append(" in line "+(s.left+1));   
                /* Check if the column number in the input is greater
                   or equal to zero. */
                if (s.right >= 0)                    
                    /* Add to the end of the StringBuffer error message
                       the column number of the error in the input. */
                    m.append(", column "+(s.right+1)+" value <"+s.value+">");
            }
        }
   
        /* Add to the end of the StringBuffer error message created in
           this method the message that was passed into this method. */
        m.append(" : "+message);
   
        /* Print the contents of the StringBuffer 'm', which contains
           an error message, out on a line. */
        //System.err.println(m);
		salida.append(m+"\n");
    }
   
    public void report_fatal_error(String message, Object info) {
        report_error(message, info);
    }


}

/** Cup generated class to encapsulate user supplied action code.*/
class CUP$OptimizacionParser1$actions {



	//////////////////////////
	//varios...
	HashMap<Integer,String> bloques=null;
	int bloque_count=1;
	
	void println(String msg){
		parser.out(msg);
	}
	//////////////////////////
	//errores
	void error(String msg, Object info){
		parser.report_error(msg,info);
	}


  private final OptimizacionParser1 parser;

  /** Constructor */
  CUP$OptimizacionParser1$actions(OptimizacionParser1 parser) {
    this.parser = parser;
  }

  /** Method with the actual generated action code. */
  public final java_cup.runtime.Symbol CUP$OptimizacionParser1$do_action(
    int                        CUP$OptimizacionParser1$act_num,
    java_cup.runtime.lr_parser CUP$OptimizacionParser1$parser,
    java.util.Stack            CUP$OptimizacionParser1$stack,
    int                        CUP$OptimizacionParser1$top)
    throws java.lang.Exception
    {
      /* Symbol object for return from actions */
      java_cup.runtime.Symbol CUP$OptimizacionParser1$result;

      /* select the action based on the action number */
      switch (CUP$OptimizacionParser1$act_num)
        {
          /*. . . . . . . . . . . . . . . . . . . .*/
          case 6: // body ::= BODY 
            {
              MyClass RESULT =null;
		int xleft = ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()).left;
		int xright = ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()).right;
		String x = (String)((java_cup.runtime.Symbol) CUP$OptimizacionParser1$stack.peek()).value;
		
						bloques.put(bloque_count,x);
						bloque_count++;
					
              CUP$OptimizacionParser1$result = parser.getSymbolFactory().newSymbol("body",3, ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), RESULT);
            }
          return CUP$OptimizacionParser1$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 5: // body_lista ::= body 
            {
              MyClass RESULT =null;

              CUP$OptimizacionParser1$result = parser.getSymbolFactory().newSymbol("body_lista",2, ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), RESULT);
            }
          return CUP$OptimizacionParser1$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 4: // body_lista ::= body_lista body 
            {
              MyClass RESULT =null;

              CUP$OptimizacionParser1$result = parser.getSymbolFactory().newSymbol("body_lista",2, ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.elementAt(CUP$OptimizacionParser1$top-1)), ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), RESULT);
            }
          return CUP$OptimizacionParser1$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 3: // app ::= body_lista 
            {
              MyClass RESULT =null;

              CUP$OptimizacionParser1$result = parser.getSymbolFactory().newSymbol("app",1, ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), RESULT);
            }
          return CUP$OptimizacionParser1$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 2: // $START ::= inicio EOF 
            {
              Object RESULT =null;
		int start_valleft = ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.elementAt(CUP$OptimizacionParser1$top-1)).left;
		int start_valright = ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.elementAt(CUP$OptimizacionParser1$top-1)).right;
		MyClass start_val = (MyClass)((java_cup.runtime.Symbol) CUP$OptimizacionParser1$stack.elementAt(CUP$OptimizacionParser1$top-1)).value;
		RESULT = start_val;
              CUP$OptimizacionParser1$result = parser.getSymbolFactory().newSymbol("$START",0, ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.elementAt(CUP$OptimizacionParser1$top-1)), ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), RESULT);
            }
          /* ACCEPT */
          CUP$OptimizacionParser1$parser.done_parsing();
          return CUP$OptimizacionParser1$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 1: // inicio ::= NT$0 app 
            {
              MyClass RESULT =null;
              // propagate RESULT from NT$0
                RESULT = (MyClass) ((java_cup.runtime.Symbol) CUP$OptimizacionParser1$stack.elementAt(CUP$OptimizacionParser1$top-1)).value;
		
						
					
              CUP$OptimizacionParser1$result = parser.getSymbolFactory().newSymbol("inicio",0, ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.elementAt(CUP$OptimizacionParser1$top-1)), ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), RESULT);
            }
          return CUP$OptimizacionParser1$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 0: // NT$0 ::= 
            {
              MyClass RESULT =null;

						this.bloques=parser.bloques;
					
              CUP$OptimizacionParser1$result = parser.getSymbolFactory().newSymbol("NT$0",4, ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), ((java_cup.runtime.Symbol)CUP$OptimizacionParser1$stack.peek()), RESULT);
            }
          return CUP$OptimizacionParser1$result;

          /* . . . . . .*/
          default:
            throw new Exception(
               "Invalid action number found in internal parse table");

        }
    }
}

