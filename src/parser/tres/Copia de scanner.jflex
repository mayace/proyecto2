package parser.tres;

import java_cup.runtime.Symbol;
import javax.swing.JTextArea;

%%

%class TresScanner
%public
%line
%column
%cup


%{
	
	
	JTextArea salida=new JTextArea();
	public void setSalida(JTextArea salida){
		this.salida=salida;
	}
	
	public JTextArea getSalida(){
		return this.salida;
	}
	
	public void out(String text){
		this.salida.append(text+"\n");
	}
	
    /* To create a new java_cup.runtime.Symbol with information about
       the current token, the token will have no value in this
       case. */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}

NEWLINE		=\n|\r|\r\n
ESPACIO     =[ \t]|{NEWLINE}
LETRA		=[a-z�A-Z�]
DIGITO		=[0-9]

ETIQUETA	=("l"|"L"){INT}
GOTO		=("GOTO"|"goto"|"Goto")
TEMPORAL	=("t"|"T"){INT}
STACK		="STACK"|"stack"|"Stack"
HEAP		="HEAP"|"heap"|"Heap"
P			="P"|"p"
H			="H"|"h"

ID			={LETRA}({LETRA}|{DIGITO}|"_")*
INT			={DIGITO}+
FLOAT		={INT}("."{INT})?
CHAR		="'"({LETRA}|{DIGITO})"'"
BOOLEAN		="true"|"false"
STRING		=\"[^\"\n\r]*\"

SIMPLE_C	="//"[^]*{NEWLINE}
/* MULTI_C		="!!!"[^]*"!!!" */

%%

<YYINITIAL>{
	{ESPACIO}		{/*nada*/}
	{SIMPLE_C}		{/*COMENTARIO*/}
	
	"<"				{return symbol(TresSym.MENOR,yytext());}
	">"				{return symbol(TresSym.MAYOR,yytext());}
	">="			{return symbol(TresSym.MAYOR_IGUAL,yytext());}
	"<="			{return symbol(TresSym.MENOR_IGUAL,yytext());}
	"!="			{return symbol(TresSym.NOIGUAL,yytext());}
	"=="			{return symbol(TresSym.IGUAL2,yytext());}
	
	":"				{return symbol(TresSym.DPUNTOS,yytext());}
	";"				{return symbol(TresSym.PCOMA,yytext());}
	"="				{return symbol(TresSym.IGUAL,yytext());}
	
	"{"				{return symbol(TresSym.LLAVE1,yytext());}
	"}"				{return symbol(TresSym.LLAVE2,yytext());}
	"("				{return symbol(TresSym.PAR1,yytext());}
	")"				{return symbol(TresSym.PAR2,yytext());}
	"["				{return symbol(TresSym.CORCHETE1,yytext());}
	"]"				{return symbol(TresSym.CORCHETE2,yytext());}
	
	"*"				{return symbol(TresSym.MULTI,yytext());}
	"+"				{return symbol(TresSym.SUMA,yytext());}
	"-"				{return symbol(TresSym.RESTA,yytext());}
	"/"				{return symbol(TresSym.DIVISION,yytext());}	
	"%"				{return symbol(TresSym.MOD,yytext());}	
	
	"void"			{return symbol(TresSym.VOID,yytext());}
	"if"			{return symbol(TresSym.IF,yytext());}
	
	{ETIQUETA}		{return symbol(TresSym.TAG,yytext());}
	{GOTO}			{return symbol(TresSym.GOTO,yytext());}
	{TEMPORAL}		{return symbol(TresSym.TEMP,yytext());}
	{P}				{return symbol(TresSym.P,yytext());}
	{H}				{return symbol(TresSym.H,yytext());}
	{STACK}			{return symbol(TresSym.STACK,yytext());}
	{HEAP}			{return symbol(TresSym.HEAP,yytext());}
	
	
	{BOOLEAN}		{return symbol(TresSym.BOOLEAN,yytext());}
	{ID}			{return symbol(TresSym.ID,yytext());}
	{INT}			{return symbol(TresSym.INT,yytext());}
	{FLOAT}			{return symbol(TresSym.FLOAT,yytext());}
	{CHAR}			{return symbol(TresSym.CHAR,yytext());}
	{STRING}		{return symbol(TresSym.STRING,yytext());}

}

<<EOF>>				{return symbol(TresSym.EOF,yytext());}

[^]		{salida.append("[Error] Illegal character <"+yytext()+"> at line "+(yyline+1)+" column " +(yycolumn+1)+"\n");}