/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.util.ArrayList;
import java.util.HashMap;
import java_cup.runtime.Symbol;

/**
 *
 * @author hp
 */
public class MyClass{
		/**
		 * Almacena el nombre y el valor de los atributos creados
		 */
		HashMap<String, Object> attribute=new HashMap<>();
				
		public MyClass(){}
		/**
		 * Instanciar con un atributo inicial...
		 * @param nombre
		 * @param valor 
		 */
		public MyClass(String nombre, Object valor){
			this.set(nombre, valor);
		}
	   /**
		* Crea | Modifica un atributo
		* @param nombre
		* @param valor 
		*/ 
	   public Object set(String nombre,Object valor){
		   return this.attribute.put(nombre, valor);
	   }
	   
	   /**
		* Devuelve el valor de un atributo
		* @param nombre
		* @return 
		*/
	   public String getString(String nombre){
		   String val=null;
		   
		   if(this.attribute.containsKey(nombre)){
			   try {
				   val = String.valueOf( this.attribute.get(nombre));
			   } catch (Exception exc) {
				   return null;
			   }
		   }
		   
		   return val;
	   }
	   /**
		* Devuelve el valor de un atributo
		* @param nombre
		* @return 
		*/
	   public Float getFloat(String nombre){
		   Float val=null;
		
		   if(this.attribute.containsKey(nombre)){
			   try {
				   val =Float.valueOf( this.attribute.get(nombre).toString());
			   } catch (Exception exc) {
				   return null;
			   }
		   }
		   
		   return val;
	   }
	   /**
		* Devuelve el valor de un atributo
		* @param nombre
		* @return null si no se encuentra el atributo ó no se puede convertir
		*/
	   public Integer getInteger(String nombre){
		   Integer val=null;
		   
		   if(this.attribute.containsKey(nombre)){
			   try {
				   val = new Integer(this.attribute.get(nombre).toString());
			   } catch (Exception exc) {
				   return null;
			   }
		   }
		   
		   return val;
		}
		 /**
		* Devuelve el valor de un atributo
		* @param nombre
		* @return null si no se encuentra el atributo ó no se puede convertir
		*/
	   public ArrayList<MyClass> getList(String nombre){
		   ArrayList<MyClass> val=null;
		   
		   if(this.attribute.containsKey(nombre)){
			   try {
				   val = (ArrayList<MyClass>) this.attribute.get(nombre);
			   } catch (Exception exc) {
				   return null;
			   }
		   }
		   
		   return val;
		}
           public ArrayList<MySym> getMySymList(String nombre){
		   ArrayList<MySym> val=null;
		   
		   if(this.attribute.containsKey(nombre)){
			   try {
				   val = (ArrayList<MySym>) this.attribute.get(nombre);
			   } catch (Exception exc) {
				   return null;
			   }
		   }
		   
		   return val;
		}
		
		/**
		* Devuelve el valor de un atributo
		* @param nombre
		* @return null si no se encuentra el atributo ó no se puede convertir
		*/
	   public ArrayList<Integer> getIntegerList(String nombre_l ,String nombre ){
		   ArrayList<MyClass> val=null;
		   
		   if(this.attribute.containsKey(nombre_l)){
			   try {
				   val = (ArrayList<MyClass>) this.attribute.get(nombre_l);
			   } catch (Exception exc) {
				   return null;
			   }
		   }
		   ArrayList<Integer> val2=new ArrayList<>();
		   for(MyClass mc : val){
				Integer v=mc.getInteger(nombre);
				if(v!=null)
					val2.add(v);
				else
					return null;		//si getInteger tira null que returne null
		   }
		   
		   return val2;
		}
		/**
		* Devuelve una lista de Flotantes
		* @param list_nombre nombre de la lista donde se encuentra los MyClass
		* @param nombre nombre del atributo que contiene los flotatnes en MyClass
		* @return null si no se encuentra el atributo ó no se puede convertir
		*/
	   public ArrayList<Float> getFloatList(String list_nombre ,String nombre ){
		   ArrayList<MyClass> val=null;
		   
		   if(this.attribute.containsKey(list_nombre)){
			   try {
				   val = (ArrayList<MyClass>) this.attribute.get(list_nombre);
			   } catch (Exception exc) {
				   return null;
			   }
		   }
		   ArrayList<Float> val2=new ArrayList<>();
		   for(MyClass mc : val){
				Float v=mc.getFloat(nombre);
				if(v!=null)
					val2.add(v);
				else
					return null;		//si getInteger tira null que returne null
		   }
		   
		   return val2;
		}
		/**
		* Devuelve una lista de Flotantes
		* @param list_nombre nombre de la lista donde se encuentra los MyClass
		* @param nombre nombre del atributo que contiene los flotatnes en MyClass
		* @return null si no se encuentra el atributo ó no se puede convertir
		*/
	   public ArrayList<String> getStringList(String list_nombre ,String nombre ){
		   ArrayList<MyClass> val=null;
		   
		   if(this.attribute.containsKey(list_nombre)){
			   try {
				   val = (ArrayList<MyClass>) this.attribute.get(list_nombre);
			   } catch (Exception exc) {
				   return null;
			   }
		   }
		   ArrayList<String> val2=new ArrayList<>();
		   for(MyClass mc : val){
				String v=mc.getString(nombre);
				if(v!=null)
					val2.add(v);
				else
					return null;		//si getInteger tira null que returne null
		   }
		   
		   return val2;
		}
		/**
		* Devuelve el valor de un atributo
		* @param nombre
		* @return 
		*/
	   public Object getObject(String nombre){
		   Object val=null;
		   
		   if(this.attribute.containsKey(nombre)){
			   try {
				   val = this.attribute.get(nombre);
			   } catch (Exception exc) {
				   return null;
			   }
		   }
		   
		   return val;
	   }
           
           public Boolean getBoolean(String nombre){
               try {
                   return Boolean.valueOf(getObject(nombre).toString());
               } catch (Exception exc) {
                   return null;
               }
           }
           
           public MyClass getMyClass(String nombre){
               try {
                   return ((MyClass)getObject(nombre));
               } catch (Exception exc) {
                   return null;
               }
           }
           
           public MySym getMySym(String nombre){
               try {
                   return ((MySym)getObject(nombre));
               } catch (Exception exc) {
                   return null;
               }
           }
           public Symbol getSymbol(String nombre){
               try {
                   return ((Symbol)getObject(nombre));
               } catch (Exception exc) {
                   return null;
               }
           }    
}
	
	
